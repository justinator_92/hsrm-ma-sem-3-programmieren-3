#!/usr/bin/env python
# -*- coding: utf-8 -*-

import string
from pathlib import Path


def read_file_as_string(path):
    return Path(path).read_text()


def count_words(path):
    content = read_file_as_string(path).split()
    dic = {}
    for word in content:
        if word not in dic:
            dic[word] = 1
        else:
            dic[word] += 1
    return dic


def count_chars(path):
    content = filter_content = filter(lambda x: x in string.ascii_letters, read_file_as_string(path))
    dic = {}
    for char in content:
        if char not in dic:
            dic[char] = 1
        else:
            dic[char] += 1
    return dic

print("count geladen")
