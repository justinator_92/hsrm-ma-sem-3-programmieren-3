#!/usr/bin/env python
# -*- coding: utf-8 -*-

import string
from pathlib import Path

DEUTSCH = "de"
ENGLISCH = "en"
language = DEUTSCH

lang_de = ("Die Datei", "hat", "Zeilen", "Wörter", "Buchstaben")
lang_en = ("The file", "contains", "lines", "words", "chars")


def read_file_as_string(path):
    return Path(path).read_text()


def chars(path):
    filter_content = filter(lambda x: x in string.ascii_letters, read_file_as_string(path))
    return len(list(filter_content))


def words(path):
    content = read_file_as_string(path)
    return len(content.split())


def lines(path):
    content = read_file_as_string(path)
    return len(content.split('\n'))


def wc(path):
    return {"chars": chars(path), "words": words(path), "lines": lines(path)}


def wc_show(path):
    dic = wc(path)
    if language == "de":
        lang = lang_de
    else:
        lang = lang_en

    content = "{} " + path + " {}\n" + \
              str(dic['lines']) + " {}\n" + \
              str(dic['words']) + " {}\n" + \
              str(dic['chars']) + " {}\n"
    print(content.format(*lang))


def set_lang(lang):
    if not lang == DEUTSCH and not lang == ENGLISCH:
        print("language:", lang, "not supported.")
        return
    global language  # change or create global variables...
    language = lang

print("wc geladen")
