#!/usr/bin/env python
# -*- coding: utf-8 -*-


from text import text


if __name__ == '__main__':
    file_path = "words.txt"
    text.wc_show(file_path)
    text.set_lang("en")
    text.wc_show(file_path)

    print(text.count_words("words1.txt"))
    print(text.count_chars("words1.txt"))
