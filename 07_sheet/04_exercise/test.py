from menge import Set, OrderedSet

a = Set([3,1,6,2,5,4])
b = OrderedSet([3,1,6,2,5,4])

print(a)

for ele in a:
    print(ele)

print(b)

for ele in b:
    print(ele)