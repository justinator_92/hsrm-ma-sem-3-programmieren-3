#!/usr/bin/env python
# -*- coding: utf-8 -*-


class Set(object):

    my_set = []

    def __init__(self, seq):
        self.my_set = seq

    def add(self, elem):
        if not type(elem) == int:
            raise Exception

        if elem in self.my_set:
            return
        self.my_set.append(elem)

    def union_update(self, seq):
        for ele in seq.my_set:
            if ele not in self.my_set:
                self.my_set.append(ele)

    def union(self, seq):
        ret = Set(self.my_set[:])
        ret.union_update(seq)
        return ret

    def remove(self, elem):
        if elem in self.my_set:
            self.my_set.remove(elem)

    def difference_update(self, seq):
        self.my_set = list(filter(lambda x: x not in seq, self.my_set))

    def difference(self, seq):
        ret = Set(self.my_set[:])
        ret.difference_update(seq)
        return ret

    def clear(self):
        self.my_set.clear()

    def size(self):
        return len(self.my_set)

    def __contains__(self, item):
        return item in self.my_set

    def __eq__(self, other):
        if not len(other) == len(self):
            return False
        for ele in self.my_set:
            if ele not in other.my_set:
                return False

        return True

    def __ne__(self, other):
        return not self == other

    def __repr__(self):
        return repr(self.my_set)

    def __add__(self, other):
        if type(other) == type(self):
            self.union_update(other)
        elif type(other) == list:
            for ele in other:
                if ele not in self.my_set:
                    self.my_set.append(ele)
        return self

    def __radd__(self, other):
        if type(other) == type(self):
            self.union_update(other)
        elif type(other) == list:
            for ele in other:
                if ele not in self.my_set:
                    self.my_set.append(ele)
        return self

    def __sub__(self, other):
        self.difference(other.my_set)
        return self

    def __len__(self):
        return len(self.my_set)

    def __iter__(self):
        self.index = 0
        return self

    def __next__(self):
        if self.index >= len(self.my_set):
            raise StopIteration
        self.index += 1
        return self.my_set[self.index - 1]


class OrderedSet(Set):

    def __init__(self, seq):
        super(OrderedSet, self).__init__(seq)

    def __iter__(self):
        self.index = 0
        return self

    def __next__(self):
        if self.index >= len(self.my_set):
            raise StopIteration
        self.index += 1
        return sorted(self.my_set)[self.index - 1]
