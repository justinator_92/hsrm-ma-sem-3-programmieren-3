#!/usr/bin/env python
# -*- coding: utf-8 -*-


import primgen
from itertools import islice

p = primgen.prim()
# for ele in range(10):
#     print(next(p), end= " ")
#
pp = primgen.prim_pair()
# for _ in range(100):
#     print(next(pp))


ip = islice(p, 0, 10)
print(list(ip))

ipp = islice(pp, 0, 100)
print(list(ipp))