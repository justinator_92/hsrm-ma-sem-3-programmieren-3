#!/usr/bin/env python
# -*- coding: utf-8 -*-


import math

def prim():
    yield 2
    count = 3

    while True:
        isPrime = True

        for x in range(2, int(math.sqrt(count) + 1)):
            if count % x == 0:
                isPrime = False
                break

        if isPrime:
            yield count

        count += 2


def prim_pair():
    p = prim()
    a = next(p)

    while True:
        b = next(p)

        if b - a == 2:
            yield (a, b)

        a = b
