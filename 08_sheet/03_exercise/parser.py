#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys


def split_line(line):
	return tuple(map(float, filter(lambda x: x != '', line.split(' ')[1:])))


def read_file(file_path):
	with open(file_path, 'r') as myfile:
		content=myfile.readlines()
	return list(map(split_line, filter(lambda x: x.startswith('v'), content)))


if __name__ == '__main__':
	if(len(sys.argv) < 1):
		print("no arguments")
	print(read_file(sys.argv[1]))

