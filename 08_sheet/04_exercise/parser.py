#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from PIL import Image


def split_line(line):
	return tuple(map(float, filter(lambda x: x != '', line.split(' ')[1:])))


def read_file(file_path):
	with open(file_path, 'r') as myfile:
		content=myfile.readlines()
	return list(map(split_line, filter(lambda x: x.startswith('v'), content)))

def draw_image(img_data):
	im = Image.new("1", (400,400))

	for point in img_data:
		x, y = int(point[1]), int(point[2])
		im.putpixel((x,y), 1)

	im.save("image.jpg")


if __name__ == '__main__':
	if(len(sys.argv) < 2):
		print("no arguments, python parser.py <*.obj file>")
		exit(1)
	file_content = read_file(sys.argv[1])
	draw_image(file_content)

