#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

def count_words(seq):
    dic = {}
    for word in seq:
        if word not in dic:
            dic[word] = 1
        else:
            dic[word] += 1
    return dic

def print_words(dic):
    max_occur = max(dic.values())
    names = list(filter(lambda (x, y): y == max_occur, dic.iteritems()))
    names = sorted(map(lambda (x, y): x, names))
    for name in names:
        print(str(max_occur) + ":" + name)

if __name__ == '__main__':
    dic = count_words(sys.argv[1:])
    print_words(dic)
