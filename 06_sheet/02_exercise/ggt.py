#!/usr/bin/python
#-*- coding: utf-8 -*-

def ggTr(x, y):
    if x == y:
        return x
    elif x > y:
        return ggTr(x-y, y)
    else:
        return ggTr(x, y-x)

def ggT(x, y):
    if y == 0:
        return x
    return ggT(y, x%y)

def ggTi(x, y):
    while x != 0:
        if x>y:
            x = x-y
        else:
            y = y-x
    return x

def ggTl(liste):
    ggt = list(map(ggT(zip(liste, liste[1:]))))
    return ggt

print(ggTr(10, 30))
print(ggT(10, 30))

lis = [1,2,3,4,5,6,7,8,9]
print(ggTl(lis))
