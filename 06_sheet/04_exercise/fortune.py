#!/usr/bin/python3
# -*- coding: utf-8 -*-

from os import listdir
from os.path import isfile, join
import random, sys, string

file_dir = "data"

# get filenames...
files = [f for f in listdir(file_dir) if isfile(join(file_dir, f))]
filter_files = list(filter(lambda x: not x.endswith('.u8') and not x.endswith('.dat'), files)) 

# read in files...
fortunes = []
for cur_file in filter_files:
    fortunes.extend([line.rstrip('\n') for line in open(join(file_dir, cur_file))])
fortunes = list(filter(lambda x: x != "%", fortunes))

if __name__ == '__main__':
    if len(sys.argv) > 1:
        fortunes = list(filter(lambda x: sys.argv[1] in x, fortunes))

    print(fortunes[random.randint(0, len(fortunes))])    
