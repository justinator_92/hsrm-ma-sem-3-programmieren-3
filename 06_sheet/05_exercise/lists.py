#!/usr/bin/python3
# -*- coding: utf-8 -*-

even_cubic = [x**3 for x in range(1, 10) if x % 2 == 0]
print(even_cubic)

def list_without_divisor(z):
    return [x for x in range(2, z) if z%x == 0]

print(list_without_divisor(123))
print(list_without_divisor(12345))
print(list_without_divisor(123456))

primes = [x for x in range(10000, 10100) if [y for y in range(2, x) if x%y == 0] == []]
print(primes)
