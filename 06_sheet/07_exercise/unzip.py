#!/usr/bin/python3
# -*- coding: utf-8 -*-

def unzip(zipped):
    ret = []
    for i in range(len(zipped[0])):
        ret.append((zipped[0][i], zipped[1][i]))
    return ret

zipped = list(zip([1,2,3,4], [5,6], [7,8]))
print(zipped)
print(list(unzip(zipped)))
