#!/usr/bin/python3
# -*- coding: utf-8 -*-

lis = [1,2,3]
i = 0
while i < len(lis):
    print(lis[i])
    i += 1

dic = {1:"eins", 2: "zwei", 3: "drei"}
for key, value in dic.items():
    print(key, value)
