#include <stdlib.h>
#include <stdio.h>

#include "linkedlist.h"

int is_empty(list * l) {
	return l == NULL;
}

list *add_first(list *l, void *data) {
	list * new = malloc(sizeof(list));
	new->data = data;
	if(is_empty(l)) return new;
	
	new->next = l;
	l->prev = new;
	return new;
}

list *add_ith(list *l, int i, void *data) {
	list * new = malloc(sizeof(list));
	new->data = data;
	if(is_empty(l)) return new;
	
	if(i == 0) {
		new->next = l;
		return new;
	}
	
	list * temp = l;
	while(i > 1) {
		temp = temp->next;
		i -= 1;
	}
	
	list * follower = temp->next;
	
	if(follower == NULL) {
		temp->next = new;
		new->prev = temp;
		return l;
	}
	
	temp->next = new;
	new->prev = temp;
	new->next = follower;
	follower->prev = new;
	
	return l; // return head of list
}

list *add_last(list *l, void *data) {
	list * new = malloc(sizeof(l));
	new->data = data;
	if(is_empty(l) == 1) return new;
	
	list * temp = l;
	while(temp->next != NULL) {
		temp = temp->next;
	}
	temp->next = new;
	new->prev = temp;
	return l;	
}

list *del_first(list *l) {
	if(is_empty(l) == 1) return NULL;
	
	list * follower = l->next;
	free(l);
	return follower;
}

list *del_ith(list *l, int i) {
	if(is_empty(l) == 1) return NULL;
	
	if(i == 0) {
		list * follower = l->next;
		free(l);
		return follower;
	}
	
	list * temp = l;
	while(i > 0) {
		temp = temp->next;
		i -= 1;
	}
	
	list * prev = temp->prev;
	list * next = temp->next;
	free(temp);
	prev->next = next;
	next->prev = prev;
	return l;	
}

list *del_last(list *l) {
	if(is_empty(l)) return NULL;
	
	list * temp = l;
	while(temp->next != NULL)
		temp = temp->next;
		
	temp->prev->next = NULL;
	free(temp);
	return l;	
}

void del_all(list *l) {
	while(!is_empty(l)) l = del_first(l);
}

void del_all_custom(list *l, void (*custom)(void *)) {
	if(is_empty(l) == 1) return;
	
	list * temp;
	while(l != NULL) {
		temp = l->next;
		custom(l);
		l = temp;
	}
}

list *copy(list *l) {
	if(is_empty(l) == 1) return NULL;
	
	list * temp = l;
	list * ret = NULL;
	while(temp != NULL) {
		ret = add_last(ret, temp->data);
		ret->next = NULL;
		temp = temp->next;
	}
	return ret;
}

list *copy_custom(list *l, void *(*custom)(void *)){
	return NULL;
}
