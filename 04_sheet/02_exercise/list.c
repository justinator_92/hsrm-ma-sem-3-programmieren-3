#include <stdio.h>
#include <stdlib.h>

#include "linkedlist.h"

void print_list_int(list * l) {
	if(is_empty(l) == 1) {
		printf("Liste ist leer!\n");
		return;
	}
	
	list * temp = l;
	while(temp != NULL) {
		printf("%d, ", *((int *)temp->data));
		temp = temp->next;
	}
	printf("\n");
}

int main(int argc, char * argv[static 1]) {
	int a = 1;
	int b = 2;
	int c = 3;
	int d = 4;
	int e = 5;
	int f = 6;
	int g = 7;	
	
	list * l = NULL;
	
	// test add_first
	printf("empty=%d\n", is_empty(l));
	l = add_first(l, (void*) &a);
	printf("empty=%d\n", is_empty(l));
	l = add_first(l, (void*) &b);
	l = add_first(l, (void*) &c);
	print_list_int(l);

	// test add_ith
	l = add_ith(l, 2, &d);
	l = add_ith(l, 0, &e);
	print_list_int(l);	
	
	// test add_last
	l = add_last(l, &f);
	l = add_last(l, &g);
	print_list_int(l);
	
	// test del_first
	l = del_first(l);
	l = del_first(l);
	print_list_int(l);
	
	// test del_ith
	l = del_ith(l, 2);
	l = del_ith(l, 0);
	print_list_int(l);
	
	// test del_last
	l = del_last(l);
	print_list_int(l);
	
	l = add_first(l, &a);
	print_list_int(l);
	
	//del_all(l); 
	del_all_custom(l, free);
	
	l = add_first(NULL, &a);
	l = add_first(l, &b);
	l = add_first(l, &c);

	print_list_int(l);
	printf("copy_list:\n");
	
	list * list_copy = copy(l);
	print_list_int(list_copy);
	del_all(l);
	print_list_int(list_copy);
	
	return 0;
}
