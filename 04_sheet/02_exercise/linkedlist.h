#ifndef _LINKEDLIST_H
#define _LINKEDLIST_H

struct _list {
	void *data;
	struct _list * next;
	struct _list * prev;
} typedef list;

int is_empty(list *l);

list *add_first(list *l, void *data);
list *add_ith(list *l, int i, void *data);
list *add_last(list *l, void *data);

list *del_first(list *l);
list *del_ith(list *l, int i);
list *del_last(list *l);

void del_all(list *l);
void del_all_custom(list *l, void (*custom)(void *));

list *copy(list *l);
list *copy_custom(list *l, void *(*custom)(void *));

#endif
