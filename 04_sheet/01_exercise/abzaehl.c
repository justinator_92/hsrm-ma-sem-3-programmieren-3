#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct _ring {
	char * name;
	struct _ring * next;
} typedef ring;

int is_empty(ring * node) {
	return node == NULL;
}

ring * leave(ring * person, int count) {
	if(is_empty(person) == 1) {
		printf("schon leer!");
		return NULL;
	}

	ring *prev;
	
	if(person->next == person) {
		printf("Remove=%s\n", person->name);
		free(person);
		printf("Ring ist jetzt leer!\n");
		return NULL;
	}
	
	while(count > 0) {
		prev = person;
		person = person->next;
		count -= 1;
	}
	
	prev->next = person->next;
	printf("Remove=%s\n", person->name);
	free(person);
	return prev;
}

ring * enter(ring * node, char * name) {
	ring * new = malloc(sizeof(struct _ring));
	new->name = name;

	if(is_empty(node) == 1) {
		new->next = new;
		return new;
	}
	
	ring * temp = node;
	while(temp->next != node)
		temp = temp->next;
		
	temp->next = new;
	new->next = node;
	
	printf("Add=%s\n", new->name);
	return new;
}

int main(int argc, char * argv[]){
	ring * act = NULL;
	printf("is_empty=%d\n",is_empty(act));
	act=enter(act,"Mark");
	printf("is_empty=%d\n",is_empty(act));
	act=enter(act,"Peter");
	act=enter(act,"Nina");
	act=enter(act,"Backes");
	act=enter(act,"Sven");
	act=enter(act,"Björn");
	act=enter(act,"Laura");

	act=leave(act,7);
	act=leave(act,7);
	act=leave(act,7);
	act=leave(act,7);
	act=leave(act,7);
	act=leave(act,7);
	act=leave(act,7);
	act=leave(act,7);

	return 0;
}
