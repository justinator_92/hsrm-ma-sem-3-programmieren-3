#!/usr/bin/python3
# -*- coding: utf-8 -*-

de2en = { 1: "one", 2 : "two", 3 : "three" }
de2en["eins"] = "one"
de2en["zwei"] = "two"
de2en["drei"] = "three"
