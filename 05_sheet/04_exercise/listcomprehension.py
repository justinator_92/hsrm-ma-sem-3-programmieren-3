#!/usr/bin/python3
# -*- coding: utf-8 -*-

lis = [x for x in range(101)]

print lis[:10]
print lis[-10:]
print lis[::10]
print lis[len(lis)/2]
print lis[3:-4:3]
print lis[::3][4:-5]

