#include <stdio.h>
#include <math.h>

const double EPS = 0.00000001;

double heron(double x) {
	double old, new, delta;
	old = (1.0 + x) / 2.0;

	do {
		new = (old + x / old) / 2.0;
		delta = fabs(old - new);
		old = new;
	} while(delta > EPS);

	return new;
}

int main(int argc, char *argv[]) {
	double x;
	if (argc > 1) { /* es gibt Parameter */
		sscanf(argv[1], "%lf", &x);
	} else { /* mit scanf */
		printf("Bitte Zahl eingeben: ");
		scanf("%lf", &x);
	}
	printf("sqrt(%f): %f\n", x, heron(x));

	return 0;
}
