#include <stdio.h>

void binary(int zahl) {
	for (int i = 31; i >= 0; i--){
		if ( (zahl >> i) & 1){
			putchar('1');
		} else {
			putchar('0');
		}
	}
	printf("\n");
}

void decimal(int n) {
    if (n / 10 != 0)
        decimal(n / 10);
    putchar((n % 10) + '0');
}

int main(void) {
	
	int zahl;
	
	printf("Eingabe: ");
	scanf("%d", &zahl);
	printf("Binär:   ");
	binary(zahl);
	printf("Dezimal: ");
	decimal(zahl);
	
	return 0;
}
