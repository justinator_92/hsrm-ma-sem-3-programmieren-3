#include <stdio.h>
#include <stdio.h>

char rot13(char c) {
	if(c >= 'a' && c <= 'z') 
		return ((c - 'a' + 13) % 26 + 'a');
	if(c >= 'A' && c <= 'Z') 
		return ((c - 'A' + 13) % 26 + 'A');
	return c;
}

int main(void) {
	char c;
	while ((c = getchar()) != EOF){
		putchar(rot13(c));
	} 
	return 0;
}
