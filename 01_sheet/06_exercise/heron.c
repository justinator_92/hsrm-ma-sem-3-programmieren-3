#include <stdio.h>
#include <math.h>

const double EPS = 0.00000001;

double heron(double x) {
	double old, new, delta;
	old = (1.0 + x) / 2.0;

	do {
		new = (old + x / old) / 2.0;
		delta = fabs(new - old);
		old = new;
	} while(delta > EPS);

	return new;
}

int main(int argc, char *argv[]) {
	double x;
	printf("Bitte Zahl eingeben: ");
	scanf("%lf", &x);
	printf("sqrt(%f): %f\n", x, heron(x));
	return 0;
}
