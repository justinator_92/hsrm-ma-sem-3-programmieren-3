#include <stdio.h>
#include <stdlib.h>

int my_str_len(char * str) {
	int i = 0;
	while(*str++) { i += 1; }
	return i;
}

int nr_of_digits(int number) {
	int digits = 0; 
	while (number != 0) { number /= 10; digits++; }
	return digits;
}

void komprimier(char * str) {
	char *ret = malloc(sizeof(char) * 100);
	char *ret_handler = ret;
	int digits, repition = 1;
	*ret_handler = *str++;  // copy the first character
	while(*str) {
		if(*str == *ret_handler) {  // if two characters appear in sequence
			repition += 1; str++;
		} else {  // new character is different from old one
			if(repition > 1) {  // was counter bigger than zero -> print counter
				digits = nr_of_digits(repition);
				snprintf(++ret_handler, sizeof(int), "%d", repition);
			} else {
				*(++ret_handler) = *str++;
			}

			repition = 1;
		}
	}
	
	if(repition > 1) {	
		snprintf(++ret_handler, sizeof(ret_handler), "%d", repition);
	}
	
	*(++ret_handler) = '\n';
	printf("%s", ret);
	free(ret);
}

int main(int argc, char *argv[static 1]) {
	if(argc <= 1) {
		printf("Please enter a string\n");
		return -1;
	}
	for(int i = 1; i < argc; i++) {
		komprimier(argv[i]);
	}
	return 0;
}
