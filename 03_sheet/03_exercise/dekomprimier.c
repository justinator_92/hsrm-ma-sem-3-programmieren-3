#include <stdio.h>
#include <stdlib.h>

const int maxDigits = 5;  // define maximum of allowed compressed letters...

int extractNextValue(char * string) {
	char * test = malloc(sizeof(char) * maxDigits);
	char * handler = test;
	if(test == NULL) exit(1);
	while(*string >= '0' && *string <= '9') *handler++ = *string++;
	int result = atoi(test);
	free(test);
	return result;
}

int determineLength(char * str) {
	int counter = 0;
	int temp;
	while(*str) {
		temp = extractNextValue(str);
		printf("%d\n", temp);
		if(temp > 0)
			counter += temp;
		str++;
	}
	return counter;
}

void dekomprimier(char * str) {
	int length = determineLength(str);
	printf("%d\n", length);
	char * ret = malloc(sizeof(char) * 100);
	char * ret_handler = ret;
	int value;
	char lastChar;
	
	while(*str) {
		lastChar = *str++;
		value = extractNextValue(str);
		while(*str >= '0' && *str <= '9') str++;
		
		if(value > 0) {  // determine an integer...
			for(int i = 0; i < value; i++) 
				*ret_handler++ = lastChar;
		} else {
			*ret_handler++ = lastChar;
		}	
	}
	*ret_handler = '\n';
	printf("%s", ret);
	free(ret);
}

int main(int argc, char *argv[static 1]) {
	if(argc <= 1) {
		printf("please enter arguments\n");
		return -1;
	}
	for(int i = 1; i < argc; i += 1) {
		dekomprimier(argv[i]);
	}
	return 0;
}
