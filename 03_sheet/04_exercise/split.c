#include <stdio.h>
#include <stdlib.h>

struct _split_struct {
  unsigned int anzahl;
  char **strings;  
} typedef split_struct;

split_struct ** splitList;

int my_str_len(char * str) {
	int count=0;
	while(*str) count+=1;
	return count;
}

int count_words(char *str, char * delim) {
	int count = 1;
	char * handler = delim;
	while(*str) {
		if(*str == *handler) handler++;
		
		if(!*handler) {
			count += 1; handler = delim;
		}
		str++;
	}
	return count;
}

int is_comp(char *s1, char *s2) {
	while(*s1) {
		if(*s1 != *s2) return 0;
		s1++; s2++;
	}
	return *s1 == *s2;
}

split_struct * split(char *str,char *delim) {
	split_struct * ret = malloc(sizeof(split_struct));
	int count = count_words(str, delim);
	printf("number of words: %d\n", count);
	ret->anzahl = count;	
	ret->strings = malloc(sizeof(char*) * count);
	int delim_len = my_str_len(delim);
	
	int lastPos, counter = 0;
	
	while(*str) {
		if(is_comp(str, delim) == 1) {
			
		}		
		counter += 1;
	}
	
	printf("%d\n", count_words(str, delim));
	return ret;
}

int main(int argc, char *argv[]) {
	if(argc < 3) {  
		printf("Zu wenige Argumente\n");
		return -1;
	}
	
	for(int i = 2; i < argc; i++) {
		split(argv[i],argv[1]);
	}
	
	return 0;
}
