#include <stdlib.h>
#include <stdio.h>
					/* a, b, c  d,  e, f, g,  h,  i, j, k, l, m */
int lookup_table[] = {-1, 1, 2, 3, -1, 1, 2, -1, -1, 2, 2, 4, 5,
					/* n,  o, p, q, r, s, t,  u, v,  w, x,  y, z */
					   5, -1, 1, 2, 6, 2, 3, -1, 1, -1, 2, -1, 2};

char toLower(char c) {
	if(c >= 'A' && c <= 'Z')
		return c + 32;
	return c;
}

char * soundex(char * handler) {
	char * ret = malloc(sizeof(char) * 7);
	char * ret_handler = ret;
	int counter = 0;
	*ret_handler++ = *handler++; 	
	while(counter < 5) {
		if(*handler) {
			int soundex = lookup_table[toLower(*handler++) - 'a'];
			if(soundex > 0 && soundex != *(ret_handler - 1) - 48) {
				sprintf(ret_handler++, "%d", soundex);
				counter += 1;
			}
		} else {
			*ret_handler++ = '0';
			counter += 1;
		}
	}	
	*ret_handler = '\n';
	return ret;
}

int main(int argc, char * argv[]) {
	int i;
	char * result;
	for(i = 1; i < argc; i++) {
		result = soundex(argv[i]);
		printf("\t\t %s:\t%s", argv[i], result);
		free(result);
	}
	return 0;
}
