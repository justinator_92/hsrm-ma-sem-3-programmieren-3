#include <stdio.h>
#include <math.h>

#define imageWidth 800
#define imageHeight 600

int image[imageWidth][imageHeight];

void fill_rect(int x1, int y1, int x2, int y2, int color) {
	for(int y = y1; y < y2 && y < imageHeight; y += 1) {
		for(int x = x1; x < x2 && x < imageWidth; x += 1) {
			image[x][y] = color;
		}
	}
}

void fill_circle(int x1, int y1, int radius, int color) {
	for(int y = y1 - radius; y < (y1 + radius); y +=1 ) {
		for(int x = x1 - radius; x < (x1 + radius); x += 1) {
			
			double dx = ((double)x) - ((double)x1);
			double dy = ((double)y) - ((double)y1);
			
			if(sqrt(pow(dx, 2) + pow(dy, 2)) < (double)radius)
				image[x][y] = color;	
		}
	}
	
	
}

void fill_background(int color) {
	for(int y = 0; y < imageHeight; y++)
		for(int x = 0; x < imageWidth; x++)
			image[x][y] = color;
}

void write_picture() {
	printf("P3\n%d %d\n255\n", imageWidth, imageHeight); // write PPM header...
	for(int y = 0; y < imageHeight; y++) {
		for(int x = 0; x < imageWidth; x++) {	
			int color = image[x][y];
			int r = (color & 0xff0000) >> 16;
			int g = (color & 0x00ff00) >> 8;
			int b = color & 0x0000ff;
			printf("%d %d %d\n", r, g, b);
		}
	}
}

int main(void) {
	
	fill_background(0xffff00);
	fill_rect(20, 20, 500, 300, 0x00ff00);
	fill_rect(600, 400, 1000, 800, 0xff0000);
	fill_circle(400, 300, 40, 0xaabbcc);
	write_picture();
	
	return 0;
}
