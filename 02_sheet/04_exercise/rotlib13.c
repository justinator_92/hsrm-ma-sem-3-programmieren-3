#include <stdio.h>

#include "rotlib.h"

int main(void) {
	char c;
	set_rotate(13);
	
	while((c = getchar()) != EOF) {
		putchar(rotate(c));
	}
	
	return 0;
}
