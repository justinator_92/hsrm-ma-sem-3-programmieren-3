#include "rotlib.h"

int rot_n = 0;

int rotate(int ch) {
	if(ch >= 'a' && ch <= 'z') 
		return ((ch - 'a' + rot_n) % 26 + 'a');
	if(ch >= 'A' && ch <= 'Z') 
		return ((ch - 'A' + rot_n) % 26 + 'A');
	return ch;
}

void set_rotate(int n) {
	rot_n = n;
}
