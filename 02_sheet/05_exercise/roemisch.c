#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char * rom[] = {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};
int dec[] = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};

int subtraction_applies(char * substr, int n);

void roemisch(int x){
	int i;
	if(x > 10000)
		return;
	
	for (i = 0; x != 0; i++) {
		while (x >= dec[i]){
			printf("%s", rom[i]);
			fflush(stdout);
			x -= dec[i];
		}
	}
	printf("\n");
}

void decimal(char * roemisch) {
	int i, sub, result;
	char* substr_helper = malloc(sizeof(char) * 2);
	i = sub = result = 0;
	
	while(*roemisch) {
		strncpy(substr_helper, roemisch, 2);
		printf("before: %s\n", roemisch);
		sub = subtraction_applies(substr_helper, i);
		printf("after: %s\n", roemisch);
		if(sub > 0) { /* subtraction case */
			result += sub;
			roemisch = roemisch + 2;
		} else if(*roemisch == *rom[i]) { 
			result += dec[i];
			roemisch++;
		} else {
			if(sub < 0) 
				i += 2;
			else 
				i += 1;
		}
	}
	free(substr_helper);
	printf("%d\n", result);
}

int subtraction_applies(char * substr, int n) {
	int i;
	for(i = n; i < 12; i++)
		if(strcmp(substr, rom[i]) == 0)
			return dec[i];	
	return -1;
}

int main(int argc, char * argv[]){
	int zahl;
	if(argc <= 1) {
		printf("Bitte eine römische oder dezimalzahl eingeben.. exit\n");
		return -1;
	}
	if((zahl = atoi(argv[1]))) {
		roemisch(9888);		
	} else {
		decimal(argv[1]);
	}
	return 0;
}

